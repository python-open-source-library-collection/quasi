import os
from sanic.testing import SanicTestClient
from quasi import quasi
from quasi.quasi import app
from quasi.client import wrap

quasi.cache_folder = "./"


def test_no_options():
    client = SanicTestClient(app)
    request, response = client.options("/quasi/options/")
    assert response.status == 200
    assert response.json == {"count": 0}


def test_changing_domain():
    client = SanicTestClient(app)
    request, response = client.options(
        "/quasi/options/", params={"domain": "http://test.com"}
    )

    assert response.status == 200
    assert response.json == {"count": 0, "domain": "http://test.com"}

    request, response = client.get("http://test.com")
    # make sure options are preservec
    request, response = client.options("/quasi/options")

    assert response.status == 200
    assert response.json == {"count": 1, "domain": "http://test.com"}
    assert request is not None


def test_changing_path_and_domain():
    fixture_path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "fixtures"
    )
    client = SanicTestClient(app)
    request, response = client.options(
        "/quasi/options/",
        params={
            "folder": fixture_path, "domain": "https://api.sunrise-sunset.org/"
        },
    )

    expected_json = {
        "results": {
            "sunrise": "7:22:26 AM",
            "sunset": "5:02:40 PM",
            "solar_noon": "12:12:33 PM",
            "day_length": "09:40:14",
            "civil_twilight_begin": "6:53:41 AM",
            "civil_twilight_end": "5:31:24 PM",
            "nautical_twilight_begin": "6:21:18 AM",
            "nautical_twilight_end": "6:03:48 PM",
            "astronomical_twilight_begin": "5:49:48 AM",
            "astronomical_twilight_end": "6:35:18 PM",
        },
        "status": "OK",
    }
    request, response = client.get("/json?lat=36.7201600&lng=-4.4203400")
    assert response.status == 200
    assert response.json == expected_json

    # make sure options are preserved
    request, response = client.options("/quasi/options")
    assert response.status == 200
    assert response.json == {
        "count": 2,
        "domain": "https://api.sunrise-sunset.org/",
        "folder": "/var/www/quasi/tests/fixtures",
    }
    assert request is not None


def test_named_request():
    client = SanicTestClient(app)
    request, response = client.options(
        "/quasi/options/",
        params={
            "reset": "true",
            "domain": "http://test.com",
            "name": "quasi-request-cache-name",
        },
    )

    assert response.status == 200
    assert response.json == {
        "count": 1,
        "domain": "http://test.com",
        "name": "quasi-request-cache-name",
    }

    request, response = client.get("http://test.com")
    # make sure options are preservec
    request, response = client.options("/quasi/options")

    assert response.status == 200
    assert response.json == {
        "count": 2,
        "domain": "http://test.com",
        "name": "quasi-request-cache-name",
    }
    assert request is not None


def test_decorator():
    client = SanicTestClient(app)
    wrap(data={"domain": "http://test.com"})
    request, response = client.options(
        "/quasi/options/", params={"domain": "http://test.com"}
    )

    assert response.status == 200
    assert response.json == {"domain": "http://test.com"}
