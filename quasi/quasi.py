import os
import requests
import requests_cache

from collections import MutableMapping
from requests_cache.backends import registry
from requests_cache.backends import BaseCache

from sanic import Sanic
from sanic import constants
from sanic import response
from version import __version__


try:
    import cPickle as pickle
except ImportError:
    import pickle


stop = False
port = os.getenv("QUASI_PORT", 8000)
debug = os.getenv("QUASI_DEBUG", True)
token = os.getenv("QUASI_TOKEN", None)
domain = os.getenv("QUASI_STUBBED_DOMAIN", "http://exampe.com/")
cache_folder = os.getenv("QUASI_STORE", "/tmp/quasi/store/")
header_overrides = {"Accept": "application/json"}


def safename(name):
    return [c for c in name if c.isalnum() or c in ("-", "_", ".")]


class FileStoreDict(MutableMapping):
    """ FileStoreDict - a dictionary like interface for a local file cache. """

    def __init__(self, namespace, collection_name="filestore_dict_data"):
        self.set_path(cache_folder)
        self.name = None
        self.mode = "json"

    def set_name(self, name):
        """Overide default hashed name"""
        self.name = name

    def set_path(self, cache_folder):
        """Change where the cached request will be stored"""
        self.path = os.path.abspath(cache_folder) + os.sep
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def set_mode(self, mode):
        """Chanege how the cached request is stored and read"""
        self.mode = mode

    def cache_file(self, key):
        print(f"cache name {self.name}")
        return os.path.join(self.path + (self.name or key))

    def __getitem__(self, key):
        filepath = self.cache_file(key)
        if not os.path.exists(filepath):
            print(f"Request not available in cache fetching from source")
            raise KeyError

        with open(filepath, "rb") as fp:
            print(f"Fetch request from cache {filepath}")
            return pickle.loads(fp.read())

    def __setitem__(self, key, item):
        with open(self.cache_file(key), "wb") as fp:
            print(f"Storing in cache {self.cache_file(key)}")
            fp.write(pickle.dumps(item))

    def __delitem__(self, key):
        os.unlink(self.path + key)

    def __len__(self):
        return len(os.listdir(self.path))

    def __iter__(self):
        for filename in os.listdir(self.path):
            with open(self.path + filename, "rb") as fp:
                yield pickle.loads(fp.read())

    def clear(self):
        for filename in os.listdir(self.path):
            os.unlink(self.path + filename)

    def __str__(self):
        return str(dict(self.items()))


class FileStoreCache(BaseCache):

    def __init__(self, namespace="requests-cache", **options):
        super(FileStoreCache, self).__init__(**options)
        self.responses = FileStoreDict(namespace, "responses")
        self.keys_map = FileStoreDict(namespace, "urls")

    def update(self, **values):
        print(values)
        if values.get("name"):
            self.responses.set_name(values.get("name") + "1")
            self.keys_map.set_name(values.get("name") + "2")

        if values.get("folder"):
            folder = values.get("folder")
            print(f"Request cache folder changed to {folder}")
            self.responses.set_path(values.get("folder"))
            self.keys_map.set_path(values.get("folder"))


registry["filestore"] = FileStoreCache
requests_cache.install_cache(
    "website_cache",
    backend="filestore",
    allowable_methods=constants.HTTP_METHODS,
    allowable_codes=(200, 201, 202),
)

OPTION = {"timeout": None}

app = Sanic()


@app.middleware("request")
async def store(request):
    """Used to inject the config into all future requests"""
    if request.args.get("reset"):
        request.app.config["quasi"] = {"count": 0}

    if request.app.config.get("quasi") is None:
        request.app.config["quasi"] = {"count": 0}

    print(request.args)
    if request.method != "OPTIONS":
        request.app.config["quasi"]["count"] += 1

    if request.method == "OPTIONS":
        if request.args.get("timeout"):
            request.app.config["quasi"]["timeout"] = request.args.get(
                "timeout", ""
            )
        if request.args.get("name"):
            request.app.config["quasi"]["name"] = request.args.get("name", "")
        if request.args.get("folder"):
            request.app.config["quasi"]["folder"] = request.args.get(
                "folder", cache_folder
            )
        if request.args.get("domain"):
            request.app.config["quasi"]["domain"] = request.args.get(
                "domain", ""
            )
        if request.args.get("mode"):
            request.app.config.args.get("mode", request.args.get("domain", ""))

        cache = requests_cache.get_cache()
        cache.update(**request.app.config.get("quasi"))


def make_request(url, source_request):
    s = requests.Session()
    req = requests.Request(
        source_request.method, url, data=source_request.body
    )
    prepped = req.prepare()
    prepped.headers = source_request.headers
    if token:
        prepped.headers["Authorization"] = "Token " + token
    prepped.headers.update(header_overrides)
    del prepped.headers["host"]
    return s.send(prepped, stream=True)


@app.route("/quasi/options/", methods=["OPTIONS"])
async def handle_options(request):
    print("handle options")
    print(request.app.config.get("quasi"))
    if request.args.get("stop"):
        stop = True
    return response.json(request.app.config.get("quasi"))


@app.route("/<path:path>", methods=constants.HTTP_METHODS)
async def handle_requests(request, path):
    url = domain
    if request.app.config["quasi"].get("domain"):
        url = request.app.config["quasi"].get("domain", domain)
    url += path
    print(f"Requesting data for {url}")
    if request.query_string:
        url += "?" + request.query_string

    remote_response = make_request(url, request)

    return response.raw(
        remote_response.content,
        headers=remote_response.headers,
        status=remote_response.status_code,
    )


def stop_event():
    print("stop event")


if __name__ == "__main__":
    print(f"Launching quasi {__version__}")
    app.run(host="0.0.0.0", port=int(port), debug=debug, stop_event=None)
