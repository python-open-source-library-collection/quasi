import requests


def wrap(**data):
    def wrap_func(func):
        def wrapper(*args, **kwargs):
            print(data)
            response = requests.options(
                "http://127.0.0.1:8000/quasi/options", data=data
            )
            response.raise_for_status()
            print('option reposne')
            print(response.json)
            func()

        return wrapper

    return wrap_func
